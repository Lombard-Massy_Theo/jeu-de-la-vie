﻿Public Class JeuDeLaVie

    'Affichage'
    Private Sub JeuDeLaVieLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        GameBoardCreate(20, 20)
        numberOfGenerationLabel.Text = 0
    End Sub

    'Click différent boutons'
    Private Sub ButtonAddOneClick(sender As Object, e As EventArgs) Handles buttonAddOne.Click 'Bouton qui ajoute une génération'
        Timer1.Stop()
        automatic.Text = "AUTO"
        NextGenerationPassage()
        GenerationAddToDisplay(1)
        NumberOfCellAliveToDisplay()
    End Sub
    Private Sub ButtonAddFiveClick(sender As Object, e As EventArgs) Handles buttonAddFive.Click 'Bouton qui ajoute cinq générations'
        Timer1.Stop()
        automatic.Text = "AUTO"
        For counter = 1 To 5
            NextGenerationPassage()
        Next
        GenerationAddToDisplay(5)
        NumberOfCellAliveToDisplay()
    End Sub
    Private Sub ButtonAddTenClick(sender As Object, e As EventArgs) Handles buttonAddTen.Click 'Bouton qui ajoute dix générations'
        Timer1.Stop()
        automatic.Text = "AUTO"
        For counter = 1 To 10
            NextGenerationPassage()
        Next
        GenerationAddToDisplay(10)
        NumberOfCellAliveToDisplay()
    End Sub
    Private Sub ButtonCleanClick(sender As Object, e As EventArgs) Handles clean.Click 'Bouton qui nettoie le tableau'
        For Each pictureBox As PictureBox In gameBoard.Controls
            pictureBox.BackColor = Color.White
        Next
        numberOfGenerationLabel.Text = 0
        numberOfCellAliveLabel.Text = 0
        Timer1.Stop()
        automatic.Text = "AUTO"
    End Sub
    Private Sub ButtonRandomClick(sender As Object, e As EventArgs) Handles random.Click 'Bouton qui génère des cellules en vie aléatoirement'
        For Each pictureBox As PictureBox In gameBoard.Controls
            pictureBox.BackColor = Color.White
        Next
        For Each pictureBox As PictureBox In gameBoard.Controls
            Randomize()
            If Rnd() > 0.25 Then
                pictureBox.BackColor = Color.White
            Else
                pictureBox.BackColor = Color.Pink
            End If
        Next
        NumberOfCellAliveToDisplay()
        Timer1.Stop()
        automatic.Text = "AUTO"
        numberOfGenerationLabel.Text = 0
    End Sub

    'Label génération'
    'Passage de gêneration, selon la taille du plateau passé en paramètre'
    Public Sub NextGenerationPassage()
        If gameBoardSize.Text Is "" Then
            CellVerification(20, 20)
        ElseIf gameBoardSize.SelectedItem.ToString Is "20x20" Then
            CellVerification(20, 20)
        ElseIf gameBoardSize.SelectedItem.ToString Is "30x30" Then
            CellVerification(30, 30)
        ElseIf gameBoardSize.SelectedItem.ToString Is "40x40" Then
            CellVerification(40, 40)
        ElseIf gameBoardSize.SelectedItem.ToString Is "40x70" Then
            CellVerification(40, 70)
        Else
            CellVerification(20, 20)
        End If
    End Sub
    Public Sub GenerationAddToDisplay(numberOfGenerationToAdd As Integer) 'Affiche le nombre de génération'
        Dim actuelNumberOfGeneration = numberOfGenerationLabel.Text
        Dim numberOfGenerationToDisplay = 0

        numberOfGenerationToDisplay = actuelNumberOfGeneration + numberOfGenerationToAdd
        numberOfGenerationLabel.Text = numberOfGenerationToDisplay
    End Sub

    'Label cellule vivante'
    Public Sub NumberOfCellAliveToDisplay() 'Affiche le nombre de cellule en vie'
        Dim numberOfCellAlive = 0
        For Each pictureBox As PictureBox In gameBoard.Controls
            If pictureBox.BackColor = Color.Pink Then
                numberOfCellAlive = numberOfCellAlive + 1
            End If
        Next
        numberOfCellAliveLabel.Text = numberOfCellAlive
    End Sub


    'Affichage, création et modification du plateau'

    Public Sub GameBoardSizeSelectedIndexChanged(sender As Object, e As EventArgs) Handles gameBoardSize.SelectedIndexChanged 'Selectionner la taille du tableau'
        If gameBoardSize.SelectedItem.ToString Is "20x20" Then
            gameBoard.Controls.Clear()
            GameBoardCreate(20, 20)
        End If
        If gameBoardSize.SelectedItem.ToString Is "30x30" Then
            gameBoard.Controls.Clear()
            GameBoardCreate(30, 30)
        End If
        If gameBoardSize.SelectedItem.ToString Is "40x40" Then
            gameBoard.Controls.Clear()
            GameBoardCreate(40, 40)
        End If
        If gameBoardSize.SelectedItem.ToString Is "40x70" Then
            gameBoard.Controls.Clear()
            GameBoardCreate(40, 70)
        End If
    End Sub
    Public Sub GameBoardCreate(line As Integer, column As Integer)

        'Création du plateau'
        For idLine = 0 To (line - 1)
            For idColumn = 0 To (column - 1)
                Dim pictureBoxtureBoxToAdd = New PictureBox
                pictureBoxtureBoxToAdd.Height = 18
                pictureBoxtureBoxToAdd.Width = 18
                gameBoard.Controls().Add(pictureBoxtureBoxToAdd)
                pictureBoxtureBoxToAdd.Location = New Point(pictureBoxtureBoxToAdd.Location.X + (idColumn) * 18, pictureBoxtureBoxToAdd.Location.Y + (idLine) * 18) 'Ajout de n fois la taille d'une pictureBoxturebox (hauteur ou longueur) pour le décalage
                pictureBoxtureBoxToAdd.BackColor = Color.White
                pictureBoxtureBoxToAdd.BorderStyle = BorderStyle.FixedSingle
                pictureBoxtureBoxToAdd.Name = String.Format("PB{0},{1}", idLine, idColumn)
                pictureBoxtureBoxToAdd.Tag = ""
                AddHandler pictureBoxtureBoxToAdd.MouseClick, AddressOf CelluleClick
            Next
        Next

    End Sub
    Public Sub CelluleClick(sender As Object, e As EventArgs) 'Choix de la cellule en vie dans le tableau'
        Dim pictureBox As PictureBox = sender

        If pictureBox.BackColor = Color.White Then
            pictureBox.BackColor = Color.Pink
        Else
            pictureBox.BackColor = Color.White 'Si la cellule est déjà en vie, cliquer la tue/rend la case blanche'
        End If
        NumberOfCellAliveToDisplay()
    End Sub



    'Programme principal'
    'Vérification si une cellule est vivante ou non, et affecte le résultat à un tableau de booléen, vivante = true, morte = false'
    Public Sub CellVerification(numberOfLine As Integer, numberOfColumn As Integer)
        Dim column = 0, line = 0, commaIndex = 0
        Dim gameBoardActualGeneration(numberOfLine, numberOfColumn) As Boolean

        For Each pictureBox As PictureBox In gameBoard.Controls
            commaIndex = pictureBox.Name.IndexOf(",")
            line = pictureBox.Name.Substring(2, commaIndex - 2)
            column = pictureBox.Name.Substring(commaIndex + 1)
            If pictureBox.BackColor = Color.Pink Then
                gameBoardActualGeneration(line, column) = True
            Else
                gameBoardActualGeneration(line, column) = False
            End If
        Next
        AdjacentCellCheck(gameBoardActualGeneration, numberOfLine, numberOfColumn)
    End Sub
    'Vérification des cellules autour d'une case, une cellule vivante entraine l'incrémentation d'un compteur'
    Public Sub AdjacentCellCheck(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim numberOfLivingCellAround As Integer = 0
        Dim gameBoardFollowingGeneration(numberOfLine, numberOfColumn) As Boolean

        For line = 0 To numberOfLine - 1
            For column = 0 To numberOfColumn - 1
                numberOfLivingCellAround = 0
                numberOfLivingCellAround = numberOfLivingCellAround + CheckTop(gameBoardActualGeneration, line, column)
                numberOfLivingCellAround = numberOfLivingCellAround + CheckTopLeft(gameBoardActualGeneration, line, column)
                numberOfLivingCellAround = numberOfLivingCellAround + CheckTopRight(gameBoardActualGeneration, line, column)
                numberOfLivingCellAround = numberOfLivingCellAround + CheckRight(gameBoardActualGeneration, line, column)
                numberOfLivingCellAround = numberOfLivingCellAround + CheckLeft(gameBoardActualGeneration, line, column)
                numberOfLivingCellAround = numberOfLivingCellAround + CheckBottom(gameBoardActualGeneration, line, column)
                numberOfLivingCellAround = numberOfLivingCellAround + CheckBottomLeft(gameBoardActualGeneration, line, column)
                numberOfLivingCellAround = numberOfLivingCellAround + CheckBottomRight(gameBoardActualGeneration, line, column)
                gameBoardFollowingGeneration(line, column) = GameBoardFollowingGenerationSet(gameBoardActualGeneration, numberOfLivingCellAround, line, column)
            Next
        Next
        GameBoardFollowingGenerationDisplay(gameBoardFollowingGeneration, numberOfLine, numberOfColumn)
    End Sub
    'Création de la prochaine génération de cellule en fonction du nombre de cellule adjacente que la cellule possede à la génération actuelle'
    Public Function GameBoardFollowingGenerationSet(gameBoardActualGeneration(,) As Boolean, numberOfLivingCellAround As Integer, line As Integer, column As Integer)
        If ((numberOfLivingCellAround = 2 Or numberOfLivingCellAround = 3) And gameBoardActualGeneration(line, column) = True) Then
            Return True
        End If

        If (numberOfLivingCellAround = 3 And gameBoardActualGeneration(line, column) = False) Then
            Return True
        End If

        If (numberOfLivingCellAround < 2 Or numberOfLivingCellAround > 3) Then
            Return False
        End If
    End Function
    Public Sub GameBoardFollowingGenerationDisplay(gameBoardFollowingGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer) 'Affichage de la nouvelle génération'
        Dim line = 0, column = 0
        For Each pictureBox As PictureBox In gameBoard.Controls
            If gameBoardFollowingGeneration(line, column) = True Then
                pictureBox.BackColor = Color.Pink
            Else
                pictureBox.BackColor = Color.White
            End If
            column = column + 1
            If column = numberOfColumn Then
                column = 0
                line = line + 1
            End If
        Next
    End Sub

    'Fonctions de vérifications des cases adjacentes, si la cellule est vivante, retourne 1 sinon retourne 0. Si la cellule n'existe pas, déclenche une exception et retourne 0'
    Function CheckTop(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine - 1, numberOfColumn) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try

        Return counter
    End Function
    Function CheckTopLeft(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine - 1, numberOfColumn - 1) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try
        Return counter
    End Function
    Function CheckTopRight(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine - 1, numberOfColumn + 1) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try

        Return counter
    End Function

    Function CheckBottom(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine + 1, numberOfColumn) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try

        Return counter
    End Function
    Function CheckBottomLeft(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine + 1, numberOfColumn - 1) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try

        Return counter
    End Function
    Function CheckBottomRight(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine + 1, numberOfColumn + 1) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try

        Return counter
    End Function

    Function CheckRight(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine, numberOfColumn + 1) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try

        Return counter
    End Function
    Function CheckLeft(gameBoardActualGeneration(,) As Boolean, numberOfLine As Integer, numberOfColumn As Integer)
        Dim counter = 0
        Try
            If gameBoardActualGeneration(numberOfLine, numberOfColumn - 1) = True Then
                counter = counter + 1
            End If
        Catch exception As Exception
            counter = counter
        End Try

        Return counter
    End Function

    'Ajout d'un Timer, qui repetera les actions toutes les 500ms'
    Private Sub Timer1Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        NextGenerationPassage()
        GenerationAddToDisplay(1)
        NumberOfCellAliveToDisplay()
    End Sub
    Public Sub ButtonAutomaticClick(sender As Object, e As EventArgs) Handles automatic.Click 'Création d'un nombre de générations de manière automatique'
        Timer1.Enabled = True
        Timer1.Interval = 500
        If automatic.Text Is "AUTO" Then
            Timer1.Start()
            automatic.Text = "Stop AUTO"
        Else
            Timer1.Stop()
            automatic.Text = "AUTO"
        End If
    End Sub


End Class
