﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class JeuDeLaVie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.buttonAddOne = New System.Windows.Forms.Button()
        Me.buttonAddFive = New System.Windows.Forms.Button()
        Me.buttonAddTen = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gameBoardSize = New System.Windows.Forms.ComboBox()
        Me.Generation = New System.Windows.Forms.Label()
        Me.numberOfGenerationLabel = New System.Windows.Forms.Label()
        Me.gameBoard = New System.Windows.Forms.Panel()
        Me.Alive = New System.Windows.Forms.Label()
        Me.numberOfCellAliveLabel = New System.Windows.Forms.Label()
        Me.clean = New System.Windows.Forms.Button()
        Me.random = New System.Windows.Forms.Button()
        Me.automatic = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'buttonAddOne
        '
        Me.buttonAddOne.BackColor = System.Drawing.Color.Lime
        Me.buttonAddOne.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.buttonAddOne.FlatAppearance.BorderSize = 0
        Me.buttonAddOne.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.buttonAddOne.Font = New System.Drawing.Font("Arial", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonAddOne.Location = New System.Drawing.Point(9, 193)
        Me.buttonAddOne.Margin = New System.Windows.Forms.Padding(0)
        Me.buttonAddOne.Name = "buttonAddOne"
        Me.buttonAddOne.Size = New System.Drawing.Size(72, 37)
        Me.buttonAddOne.TabIndex = 7
        Me.buttonAddOne.Text = "+1"
        Me.buttonAddOne.UseVisualStyleBackColor = False
        '
        'buttonAddFive
        '
        Me.buttonAddFive.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.buttonAddFive.BackColor = System.Drawing.Color.Cyan
        Me.buttonAddFive.FlatAppearance.BorderSize = 0
        Me.buttonAddFive.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.buttonAddFive.Font = New System.Drawing.Font("Arial", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonAddFive.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.buttonAddFive.Location = New System.Drawing.Point(95, 193)
        Me.buttonAddFive.Margin = New System.Windows.Forms.Padding(0)
        Me.buttonAddFive.Name = "buttonAddFive"
        Me.buttonAddFive.Size = New System.Drawing.Size(72, 37)
        Me.buttonAddFive.TabIndex = 8
        Me.buttonAddFive.Text = "+5"
        Me.buttonAddFive.UseVisualStyleBackColor = False
        '
        'buttonAddTen
        '
        Me.buttonAddTen.BackColor = System.Drawing.Color.Red
        Me.buttonAddTen.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.buttonAddTen.FlatAppearance.BorderSize = 0
        Me.buttonAddTen.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.buttonAddTen.Font = New System.Drawing.Font("Arial", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonAddTen.Location = New System.Drawing.Point(180, 193)
        Me.buttonAddTen.Margin = New System.Windows.Forms.Padding(0)
        Me.buttonAddTen.Name = "buttonAddTen"
        Me.buttonAddTen.Size = New System.Drawing.Size(72, 37)
        Me.buttonAddTen.TabIndex = 9
        Me.buttonAddTen.Text = "+10"
        Me.buttonAddTen.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 12.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(266, 28)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Sélection taille Plateau"
        '
        'gameBoardSize
        '
        Me.gameBoardSize.FormattingEnabled = True
        Me.gameBoardSize.Items.AddRange(New Object() {"20x20", "30x30", "40x40", "40x70"})
        Me.gameBoardSize.Location = New System.Drawing.Point(25, 91)
        Me.gameBoardSize.Name = "gameBoardSize"
        Me.gameBoardSize.Size = New System.Drawing.Size(191, 24)
        Me.gameBoardSize.TabIndex = 11
        '
        'Generation
        '
        Me.Generation.AutoSize = True
        Me.Generation.Font = New System.Drawing.Font("Arial Black", 10.2!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Generation.Location = New System.Drawing.Point(66, 131)
        Me.Generation.Name = "Generation"
        Me.Generation.Size = New System.Drawing.Size(124, 24)
        Me.Generation.TabIndex = 14
        Me.Generation.Text = "Génération :"
        '
        'numberOfGenerationLabel
        '
        Me.numberOfGenerationLabel.AutoSize = True
        Me.numberOfGenerationLabel.Font = New System.Drawing.Font("Arial", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numberOfGenerationLabel.Location = New System.Drawing.Point(109, 155)
        Me.numberOfGenerationLabel.Name = "numberOfGenerationLabel"
        Me.numberOfGenerationLabel.Size = New System.Drawing.Size(26, 29)
        Me.numberOfGenerationLabel.TabIndex = 15
        Me.numberOfGenerationLabel.Text = "0"
        '
        'gameBoard
        '
        Me.gameBoard.AutoSize = True
        Me.gameBoard.Location = New System.Drawing.Point(294, 12)
        Me.gameBoard.Name = "gameBoard"
        Me.gameBoard.Size = New System.Drawing.Size(472, 406)
        Me.gameBoard.TabIndex = 16
        '
        'Alive
        '
        Me.Alive.AutoSize = True
        Me.Alive.Font = New System.Drawing.Font("Arial", 10.2!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Alive.Location = New System.Drawing.Point(21, 318)
        Me.Alive.Name = "Alive"
        Me.Alive.Size = New System.Drawing.Size(218, 19)
        Me.Alive.TabIndex = 17
        Me.Alive.Text = "Nombre de cellule vivante :"
        '
        'numberOfCellAliveLabel
        '
        Me.numberOfCellAliveLabel.AutoSize = True
        Me.numberOfCellAliveLabel.Font = New System.Drawing.Font("Arial", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numberOfCellAliveLabel.Location = New System.Drawing.Point(109, 337)
        Me.numberOfCellAliveLabel.Name = "numberOfCellAliveLabel"
        Me.numberOfCellAliveLabel.Size = New System.Drawing.Size(26, 29)
        Me.numberOfCellAliveLabel.TabIndex = 18
        Me.numberOfCellAliveLabel.Text = "0"
        '
        'clean
        '
        Me.clean.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.clean.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.clean.FlatAppearance.BorderSize = 2
        Me.clean.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.clean.Font = New System.Drawing.Font("Arial", 13.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clean.ForeColor = System.Drawing.SystemColors.Control
        Me.clean.Location = New System.Drawing.Point(36, 369)
        Me.clean.Name = "clean"
        Me.clean.Size = New System.Drawing.Size(191, 58)
        Me.clean.TabIndex = 19
        Me.clean.Text = "Clean / Reset"
        Me.clean.UseVisualStyleBackColor = False
        '
        'random
        '
        Me.random.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.random.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.random.FlatAppearance.BorderSize = 2
        Me.random.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.random.Font = New System.Drawing.Font("Arial", 13.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.random.ForeColor = System.Drawing.SystemColors.Control
        Me.random.Location = New System.Drawing.Point(36, 433)
        Me.random.Name = "random"
        Me.random.Size = New System.Drawing.Size(191, 59)
        Me.random.TabIndex = 20
        Me.random.Text = "Random"
        Me.random.UseVisualStyleBackColor = False
        '
        'automatic
        '
        Me.automatic.BackColor = System.Drawing.SystemColors.ControlDark
        Me.automatic.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.automatic.FlatAppearance.BorderSize = 2
        Me.automatic.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.automatic.Font = New System.Drawing.Font("Arial", 13.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.automatic.ForeColor = System.Drawing.SystemColors.Control
        Me.automatic.Location = New System.Drawing.Point(36, 247)
        Me.automatic.Name = "automatic"
        Me.automatic.Size = New System.Drawing.Size(191, 58)
        Me.automatic.TabIndex = 21
        Me.automatic.Text = "AUTO"
        Me.automatic.UseVisualStyleBackColor = False
        '
        'Timer1
        '
        '
        'JeuDeLaVie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(809, 559)
        Me.Controls.Add(Me.automatic)
        Me.Controls.Add(Me.random)
        Me.Controls.Add(Me.clean)
        Me.Controls.Add(Me.numberOfCellAliveLabel)
        Me.Controls.Add(Me.Alive)
        Me.Controls.Add(Me.gameBoard)
        Me.Controls.Add(Me.numberOfGenerationLabel)
        Me.Controls.Add(Me.Generation)
        Me.Controls.Add(Me.gameBoardSize)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.buttonAddTen)
        Me.Controls.Add(Me.buttonAddFive)
        Me.Controls.Add(Me.buttonAddOne)
        Me.Name = "JeuDeLaVie"
        Me.Text = "Jeu de la vie"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents buttonAddOne As Button
    Friend WithEvents buttonAddFive As Button
    Friend WithEvents buttonAddTen As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents gameBoardSize As ComboBox
    Friend WithEvents Generation As Label
    Friend WithEvents numberOfGenerationLabel As Label
    Friend WithEvents gameBoard As Panel
    Friend WithEvents Alive As Label
    Friend WithEvents numberOfCellAliveLabel As Label
    Friend WithEvents clean As Button
    Friend WithEvents random As Button
    Friend WithEvents automatic As Button
    Friend WithEvents Timer1 As Timer
End Class
